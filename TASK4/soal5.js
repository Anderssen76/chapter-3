/* #5 Buat function untuk menghitung BMI (Body Mass Index)
- BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall
*/

function bmi(mass,height){
const bmi = mass / (height * height);
return bmi;
}
const stevenmass = 78;
const stevenheight= 1.69;
const stevenbmi = bmi(stevenmass,stevenheight);
console.log("BMI Steven = ", stevenbmi);

const Billmass = 92;
const Billheight= 1.95;
const Billbmi = bmi(Billmass,Billheight);
console.log("BMI Bill = ", Billbmi);

