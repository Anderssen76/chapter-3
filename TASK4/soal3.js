// #3 Mengambil beberapa kata terakhir dalam string
const message = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";
const kata = message.split(" ");
const posisi = 4;
const Output = kata.slice(-posisi).join(" ");
console.log(Output);

/* 
Output: belajar Javascript sangat menyenangkan
*/